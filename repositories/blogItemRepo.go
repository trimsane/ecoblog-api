package repositories

import (
	"ecoblog-api/models"
	"errors"
	"github.com/fatih/structs"
	"github.com/mitchellh/mapstructure"
	"sort"
	"strings"
	"sync"
)

var (
	ErrNotFound = errors.New("item was not found")
)

type Query func(models.BlogItem) (bool, error)

type BlogItemRepository interface {
	Exec(choose Query, action Query, limit int, mode ReadMode) error

	SelectMany(skip int, limit int, fields []string) ([]models.BlogItem, error)
	Search(pattern string, limit int, fields []string) ([]models.BlogItem, error)
	GetByID(id string, fields []string) (models.BlogItem, error)
	Count() (int, error)
}

type blogItemMemRepository struct {
	source        map[string]models.BlogItem
	indexedByDate []models.BlogItem
	mu            sync.RWMutex
}

func NewBlogItemMemoryRepository(source map[string]models.BlogItem) BlogItemRepository {
	var indexedByDate []models.BlogItem

	for _, value := range source {
		indexedByDate = append(indexedByDate, value)
	}

	sort.Slice(indexedByDate, func(i, j int) bool {
		return indexedByDate[i].CreatedAt < indexedByDate[j].CreatedAt
	})

	return &blogItemMemRepository{
		source:        source,
		indexedByDate: indexedByDate,
	}
}

func (r *blogItemMemRepository) Exec(query Query, action Query, limit int, mode ReadMode) error {

	if mode == ReadOnlyMode {
		r.mu.RLock()
		defer r.mu.RUnlock()
	} else {
		r.mu.Lock()
		defer r.mu.Unlock()
	}

	loops := 0
	for _, item := range r.indexedByDate {
		ok, err := query(item)

		if err != nil {
			return err
		}

		if !ok {
			continue
		}

		isPossible, err := action(item)

		if err != nil {
			return err
		}

		if isPossible {
			loops++
			if loops >= limit {
				break
			}
		}
	}

	return nil
}

func (r *blogItemMemRepository) SelectMany(skip int, limit int, fields []string) ([]models.BlogItem, error) {
	field := newFieldQuery(fields)

	skipped := 0
	isFound := skipped == skip

	err := r.Exec(func(item models.BlogItem) (bool, error) {
		if isFound {
			return true, nil
		}

		if skipped == skip {
			isFound = true
			return true, nil
		}

		skipped++
		return false, nil
	}, field.query, limit, ReadOnlyMode)

	return field.items, err
}

func (r *blogItemMemRepository) Search(pattern string, limit int, fields []string) ([]models.BlogItem, error) {
	field := newFieldQuery(fields)

	err := r.Exec(func(item models.BlogItem) (bool, error) {
		return strings.Contains(item.Title, pattern), nil
	}, field.query, limit, ReadOnlyMode)

	return field.items, err
}

func (r *blogItemMemRepository) GetByID(id string, fields []string) (models.BlogItem, error) {
	if blogItem, ok := r.source[id]; ok {
		return blogItem, nil
	} else {
		return models.BlogItem{}, ErrNotFound
	}
}

func (r *blogItemMemRepository) Count() (int, error) {
	return len(r.source), nil
}

type filteringQuery struct {
	items  []models.BlogItem
	fields []string
}

func newFieldQuery(fields []string) filteringQuery {
	return filteringQuery{
		fields: fields,
	}
}

func (q *filteringQuery) query(item models.BlogItem) (bool, error) {

	filtered, err := filterStructFields(item, q.fields)
	if err != nil {
		return false, err
	}

	q.items = append(q.items, filtered)
	return true, nil
}

func filterStructFields(item models.BlogItem, fields []string) (models.BlogItem, error) {
	if len(fields) == 0 {
		return item, nil
	}

	decoded := structs.Map(item)

	filtered := map[string]interface{}{}
	for _, field := range fields {
		if val, ok := decoded[field]; ok {
			filtered[field] = val
		}
	}

	if len(filtered) == 0 {
		return item, nil
	}

	filteredItem := models.BlogItem{}

	err := mapstructure.Decode(filtered, &filteredItem)
	return filteredItem, err
}
