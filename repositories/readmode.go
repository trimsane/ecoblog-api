package repositories

type ReadMode = int

const (
	ReadOnlyMode ReadMode = iota
	ReadWriteMode
)