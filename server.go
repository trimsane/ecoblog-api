package main

import (
	"ecoblog-api/api"
	"ecoblog-api/dataSources"
	"ecoblog-api/repositories"
	"github.com/kataras/iris"
	"github.com/kataras/iris/hero"
	"log"
	"net/http"
	"time"
)

func newApp () *iris.Application {
	app := iris.Default()
	heroDI := hero.New()

	repo := repositories.NewBlogItemMemoryRepository(dataSources.GenerateData(100))
	heroDI.Register(repo)

	app.Configure(api.NewRouter(heroDI))

	return app
}

func main() {
	app := newApp()

	srv := &http.Server{
		Addr: ":7070",
		ReadTimeout: 10 * time.Second,
		WriteTimeout: 10 * time.Second,
		MaxHeaderBytes: 1 << 20, // 1mb
	}

	err := app.Run(
		iris.Server(srv),
	)

	if err != nil {
		log.Fatal(err)
	}
}