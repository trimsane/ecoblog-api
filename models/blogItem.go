package models

type BlogItem struct {
	Id string `json:"id,omitempty"`

	Title string `json:"title,omitempty"`

	Intro string `json:"intro,omitempty"`
	Content string `json:"content,omitempty"`
	CreatedAt int64 `json:"createdAt,omitempty"`
}