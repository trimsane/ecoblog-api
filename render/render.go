package render

import (
	"github.com/kataras/iris"
)

func Error(ctx iris.Context, statusCode int, e error) {
	ctx.JSON(iris.Map{
		"error": e.Error(),
	})
	ctx.StatusCode(statusCode)
}

func JSON(ctx iris.Context, status int, payload interface{}) {
	ctx.StatusCode(status)
	ctx.JSON(iris.Map{
		"payload": payload,
	})
}