package api

import (
	"ecoblog-api/render"
	"ecoblog-api/repositories"
	"fmt"
	"github.com/kataras/iris"
	"github.com/kataras/iris/core/errors"
	"strings"
)

const (
	paramSearchPattern = "query"
	paramLimit         = "limit"
	paramSkip          = "skip"
	paramID            = "id"
	paramFields        = "fields"
)

func formatMissParamErr(paramName string) string {
	return fmt.Sprintf("empty required parameter %s", paramName)
}

var (
	ErrEmptyIdParam     = errors.New(formatMissParamErr(paramID))
	ErrEmptySearchParam = errors.New(formatMissParamErr(paramSearchPattern))
)

func getBlogById(ctx iris.Context, repo repositories.BlogItemRepository) {
	blogId := ctx.Params().Get(paramID)

	if blogId == "" {
		render.Error(ctx, iris.StatusBadRequest, ErrEmptyIdParam)
		return
	}

	fields := getUrlFieldsParam(ctx)

	item, err := repo.GetByID(blogId, fields)

	if err != nil {
		render.Error(ctx, iris.StatusBadRequest, err)
		return
	}

	render.JSON(ctx, iris.StatusOK, item)
}

func getAllBlogs(ctx iris.Context, repo repositories.BlogItemRepository) {
	skip := urlGetPositiveInt(ctx, paramSkip, 0)
	limit := urlGetPositiveInt(ctx, paramLimit, 10)
	fields := getUrlFieldsParam(ctx)
	
	items, err := repo.SelectMany(skip, limit, fields)

	if err != nil {
		render.Error(ctx, iris.StatusInternalServerError, err)
		return
	}

	if items == nil {
		render.Error(ctx, iris.StatusBadRequest, nil)
		return
	}

	total, err := repo.Count()

	if err != nil {
		render.Error(ctx, iris.StatusInternalServerError, err)
		return
	}

	render.JSON(ctx, iris.StatusOK, iris.Map{
		"total":  total,
		"result": items,
	})
}

func searchAllBlogs(ctx iris.Context, repo repositories.BlogItemRepository) {
	searchPattern := ctx.URLParam(paramSearchPattern)
	limit := urlGetPositiveInt(ctx, paramLimit, 10)
	fields := getUrlFieldsParam(ctx)

	if searchPattern == "" {
		render.Error(ctx, iris.StatusBadRequest, ErrEmptySearchParam)
		return
	}

	items, err := repo.Search(searchPattern, limit, fields)

	if err != nil {
		render.Error(ctx, iris.StatusBadRequest, err)
		return
	}

	render.JSON(ctx, iris.StatusOK, items)
}

func urlGetPositiveInt(ctx iris.Context, param string, def int) int {
	val := ctx.URLParamIntDefault(param, def)

	if val <= 0 {
		return def
	} else {
		return val
	}
}

func getUrlFieldsParam(ctx iris.Context) []string {
	fieldsParam := ctx.URLParam(paramFields)
	results := strings.Split(fieldsParam, ",")

	var formatted []string
	for _, value := range results {
		if value == "" {
			continue
		}

		formatted = append(formatted, strings.Title(value))
	}

	return formatted
}
