package api

import (
	"github.com/iris-contrib/middleware/cors"
	"github.com/kataras/iris"
	"github.com/kataras/iris/hero"
)

func NewRouter(heroDI *hero.Hero) iris.Configurator {

	return func(app *iris.Application) {
		crs := cors.New(cors.Options{
			AllowedOrigins: []string{"*"},
			AllowedMethods: []string{
				iris.MethodOptions,
				iris.MethodGet,
			},
		})

		apiRoot := app.Party("/api/v1", crs).AllowMethods(iris.MethodOptions)
		{
			apiRoot.Get("/ping", ping)

			apiRoot.PartyFunc("/blog-items", func(r1 iris.Party) {
				r1.Get("/", heroDI.Handler(getAllBlogs))
				r1.Get("/{id: string}", heroDI.Handler(getBlogById))
				r1.Get("/search", heroDI.Handler(searchAllBlogs))
			})
		}
	}
}
