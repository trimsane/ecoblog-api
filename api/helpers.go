package api

import (
	"github.com/kataras/iris"
)

func ping (ctx iris.Context) {
	ctx.JSON(iris.Map{
		"message": "pong",
	})
}