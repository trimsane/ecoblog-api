# EcoBlog api


### How to run:

Go to your $GOPATH 

```console
    git clone https://trimsane@bitbucket.org/trimsane/ecoblog-api.git    
    
    cd echoblog-api
    
    go get
    
    go build server.go
        
    exec ecoblog-api
```

###Api reference

- GET /blog-items/{id}
    - url params:  
        - filter?=field1,field2

- GET /blog-items/
    - url params:
        - limit?=10
        - skip?=0
        - filter?=field1,field2

- GET /blog-items/search/
    - url params:
        - query=titlename
        - limit?=10
        - filter?=field1,field2
