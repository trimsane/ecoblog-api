package dataSources

import (
	"ecoblog-api/models"
	"fmt"
	"github.com/satori/go.uuid"
	"time"
)

func GenerateData(count int) map[string]models.BlogItem  {
	data := map[string]models.BlogItem{}

	for i := 1; i <= count; i++ {
		itemId, _ := uuid.NewV4()
		unixTimeStamp := time.Now().Unix() + int64(i)

		idStr := itemId.String()
		data[idStr] = models.BlogItem{
			Id:        idStr,
			Title:     fmt.Sprint("Header ", i),
			Intro:     intro,
			Content:   content,
			CreatedAt: unixTimeStamp,
		}
	}

	return data
}

const (
	intro = `
Lorem ipsum dolor sit amet, platea blandit vitae eros condimentum quis. 
Odio praesent tellus molestie, ac ipsum eu eros, massa fermentum, et nec in eu amet, 
facilisis nulla cras. Diam excepturi dolor nunc, odio ultricies curabitur egestas odio, 
molestie iaculis urna pede amet vel, ad etiam vitae etiam ante, feugiat erat nec laudantium consequat. 
Neque accumsan metus nullam blandit gravida vel, suspendisse tempor sociosqu vel libero. 
Placerat pellentesque vitae, eget sed accumsan et labore pellentesque, urna sed vel facilisi.

Diam aliquam diam sed at, sed eu sem wisi, eu quisque, non congue tincidunt vehicula sollicitudin libero tellus.
Purus elementum vitae aliquam, nulla eros, lectus arcu lorem luctus. 
Ipsum mattis hac in aliquet eleifend, ut interdum, velit phasellus proin, pellentesque lorem placerat ac. 
Sed illum porttitor dolor justo, viverra congue commodo phasellus. Nibh curabitur orci feugiat.
`

	content = `
Habitasse eget mattis egestas venenatis cras felis, platea et proin, aenean vel, fringilla aliquam
dignissim sodales urna. Donec per wisi, commodo neque erat est vitae elit, fermentum neque orci pulvinar,
nec pellentesque fames turpis a. Lacus dui justo, sed dui dui, lectus nec sit leo, consectetuer sagittis proin sed.
Tempor amet vitae ultricies sit, adipiscing vitae eligendi dui, elementum neque, laoreet vestibulum gravida laoreet
placerat. Malesuada sit, neque non sed leo ac, sed amet, sit fusce curabitur commodo id pellentesque ligula, pede pede 
tellus. Deserunt taciti torquent eget molestie vivamus, rutrum lorem eget fames, natoque vel est purus sed,
tincidunt tempus est aliquet eu incididunt ut. Integer elementum varius turpis nisl sed.

Mollis elit ac nisl amet, varius augue nullam nulla tempor proin turpis, enim porttitor potenti curabitur,
mauris leo mauris purus, amet proin sed elementum. Eget condimentum felis, magna nec ut vestibulum.
Sem praesent erat tristique, fusce sit lacus amet laoreet aliquam scelerisque, odio nec convallis, eget per sed.
Massa nullam est tristique pede sem, tellus phasellus commodo viverra wisi ultricies sit. Dictumst tincidunt,
amet nunc elit nullam sodales, diam dui, in in. Non pellentesque scelerisque placerat, aliquam vel odio dolor libero at in,
consequat duis curabitur sagittis et vel, ante at ut mi, ut eu ac sed in. Urna turpis pulvinar mollis.
`
)